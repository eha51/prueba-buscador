import React from "react"
import {
  BrowserRouter as Router,
  Routes,
  Route
} from 'react-router-dom'
import Home from '../components/home';
import Products from "../components/products";
import ViewProduct from "../components/viewProduct";

const AppRoutes = () => {
  return (
    <Router>
      <Routes>
        <Route 
          path="/view-product"
          element={<ViewProduct />}
        />
        <Route 
          path="/"
          element={<Home />}
        />
        <Route 
          path="*"
          element={<Home />}
        />
      </Routes>
      <Products />
    </Router>
  )
}

export default AppRoutes;