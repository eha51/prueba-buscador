import React, { useContext, useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux";
import { MainContext } from "../contexts/HomeContext";
import { useNavigate } from 'react-router-dom'; 
import { setListProductsAction, setSelectProductAction } from "../redux/actions/productsAction";

const Suggestions = () => {

  const { suggestions } = useSelector(state =>  state.products)
  const [links,setLinks] = useState([]);
  const navigate = useNavigate();
  const { handleIsSearch } = useContext(MainContext);
  const dispatch = useDispatch();

  useEffect(() => {
    setLinks(suggestions);
  },[suggestions])

  const handleLink = (product) => {
    handleIsSearch(false);
    dispatch(setSelectProductAction(product))
    dispatch(setListProductsAction([]))
    navigate('/view-product')
  }

  return (
    <>
      {
        links && links.length > 0 ? (
          <section className="suggestions">
            <p className="suggestionstext">
              Sugerencias: 
              {
                links.map((link,index) => {
                  return <span className="suggestionslink" onClick={() => {
                    handleLink(link)
                  }} key={index}>{link?.name}</span>
                })
              }
            </p>
          </section>
        ): ''
      }
    </>
  )
}


export default Suggestions;