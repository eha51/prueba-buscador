import React, { useContext } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from 'react-router-dom';
import { MainContext } from "../contexts/HomeContext";
import { setListProductsAction, setSelectProductAction } from "../redux/actions/productsAction";

const Product = React.memo(({
  product
}) => {

  const navigate = useNavigate();
  const { handleIsSearch } = useContext(MainContext);
  const dispatch = useDispatch();

  const handleNavigate = () => {
    handleIsSearch(false);
    dispatch(setSelectProductAction(product))
    dispatch(setListProductsAction([]))
    navigate('/view-product')
  }

  return (
    <article className="product-container">
      <div className="productimg" onClick={handleNavigate}>
        <img src={product?.picture} alt={product?.name} width="80px"/>
      </div>
      <h1 className="producttitle" onClick={handleNavigate}>{product?.name}</h1>
      <p className="productdescription">{product?.about}</p> 
    </article>
  );

});

export default Product;