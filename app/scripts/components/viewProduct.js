import React from "react";
import { useSelector } from "react-redux";
import { convertCurrency } from "../helpers/currency";

const ViewProduct = () => {

  const { product } = useSelector(state => state.products)

  return (
    <section id="viewproduct">
      <div className="content">
        <section className="viewproduct-container">
          <article className="viewproductimg">
            <img src={product?.picture} alt={product?.name} />
          </article>
          <article className="viewproductinfo">
            <h1 className="viewproducttitle">{product?.name}</h1>
            <p className="viewproductdescription">{product?.about}</p>
            <p className="viewproductprice">{convertCurrency(product?.price)}</p>
          </article>
        </section>
      </div>
    </section>
  );
};

export default ViewProduct;
