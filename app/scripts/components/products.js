import React, { useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { MainContext } from "../contexts/HomeContext";
import { getProductsMiddleware } from "../redux/middleware/productsMiddleware";
import Product from "./product";
import Suggestions from "./suggestions";

const Products = () => {

  const { search } = useContext(MainContext);
  const dispatch = useDispatch();
  const { list } = useSelector(state => state.products);
  const [listProducts,setListProducts] = useState([]);

  useEffect(() => {
    if(search && search.length > 0) {
      dispatch(getProductsMiddleware(search))
    }
    else {
      setListProducts([])
    }
  },[search]);

  useEffect(() => {
    setListProducts(list || []);
  },[list])

  return (
    <>
      {
        listProducts && listProducts.length > 0 ? (
         <main className="products-container">
            <Suggestions />
            <section className="productsitems">
              {
                listProducts.map((product,index) => {
                  return <Product 
                        key={index} 
                        product={product}
                      />;
                })
              }
            </section>
         </main>
        ):''
      }
    </>
  )
}

export default Products;