export const types = {
  productsSetListProducts: '[Products] Set List Products',
  productsSetSelectProduct: '[Products] Set Select Product',
  productsErrors: '[Products] Errors'
}