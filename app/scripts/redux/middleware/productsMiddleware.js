import { typesRequestErrors } from "../../constants/errorConstant"
import { setErrorsProductsAction, setListProductsAction } from "../actions/productsAction"
import { getProductsService } from "../services/productsService"


export const getProductsMiddleware = (search) => {
  return (dispatch) => {

    getProductsService(search)
    .then((data) => {
      dispatch(setListProductsAction(data.products,data.suggestions));
    })
    .catch((error) => {
      dispatch(setErrorsProductsAction(
        typesRequestErrors.getListProducts,
        true,
        error
      ))
    })
    .finally(() => {

    })
  }
}