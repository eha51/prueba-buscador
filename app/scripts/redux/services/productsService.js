import axios from "axios";
import { env } from "../../environment/environment";
import { handleError } from "../../helpers/handleError"


export const getProductsService = async(search) => {
  try {
    const url = env.getProducts;

    const {data:res} = await axios.get(
      `${url}?search=${search}`,
    );

    const { products,suggestions } = res;


    return {
      products,
      suggestions
    }

  } catch (error) {
    const { status,data } = error.response || { status: null, data:null };
    throw new Error(handleError(status,data))
  }
}