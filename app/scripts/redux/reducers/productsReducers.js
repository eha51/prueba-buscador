import { typesRequestErrors } from "../../constants/errorConstant";
import { types } from "../types/types";

const initialState = {
  list: [],
  suggestions: [],
  product: null,
  errors: {
    [typesRequestErrors.getListProducts]: {
      isError: false,
      msg: null
    }
  }
};

export const productsReducer = (state = initialState, action = {}) => {
  switch(action.type) {
    case types.productsSetListProducts:
      return {
        ...state,
        list: action.payload.products,
        suggestions: action.payload.suggestions
      }
    case types.productsSetSelectProduct:
      return {
        ...state,
        product: action.payload.product
      }
    case types.productsErrors:
      return productsTypeErrors(state,action)
    default:
      return state;
  }
};

const productsTypeErrors = (state,action) => {
  switch(action.typeError) {
    case typesRequestErrors.getListProducts:
      return {
        ...state,
        errors: {
          ...state.errors,
          [typesRequestErrors.getListProducts]: {
            isError: action.payload.isError,
            msg: action.payload.msg
          }
        }
      }
    default:
      return state;
  }
}