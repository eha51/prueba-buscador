import { types } from "../types/types"

export const setListProductsAction = (products,suggestions) => {
  return {
    type: types.productsSetListProducts,
    payload: {
      products,
      suggestions
    }
  }
}

export const setErrorsProductsAction = (typeError,isError,msg) => {
  return {
    type: types.productsErrors,
    typeError,
    payload: {
      isError,
      msg
    }
  }
}

export const setSelectProductAction = (product) => {
  return {
    type: types.productsSetSelectProduct,
    payload: {
      product
    }
  }
}