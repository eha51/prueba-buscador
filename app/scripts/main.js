/**
 * The Initial React Setup file
 * ...
 * 
 * === CSS
 * The stylesheets are handled seperately using the gulp sass rather than importing them directly into React.
 * You can find these in the ./app/sass/ folder
 * 
 * == JS
 * All files in here start from this init point for the React Components.
 *  
 * 
 * Firstly we need to import the React JS Library
 */
import React, { useState } from 'react';
import ReactDOM from 'react-dom';

import Menu from './components/menu';
import { MainContext } from './contexts/HomeContext';
import { Provider } from 'react-redux';
import { store } from './redux/store/store';
import AppRoutes from './routes/AppRoutes';
import Products from './components/products';

const App = () => {

    const [search,setSearch] = useState(null);
    const [isSearch,setIsSearch] = useState(false);


    const handleIsSearch = (state) => {
        setIsSearch(state);
    }

    /**
     * Shows or hides the search container
     * @memberof Main
     * @param e [Object] - the event from a click handler
     */
    const showSearchContainer = (e) => {
        e.preventDefault();
        setIsSearch(state => !state);
    }

    return (
        <Provider store={store}>
            <div className="App">
                <MainContext.Provider value={{
                    search: search,
                    handleIsSearch
                }}>
                    <Menu 
                    showSearchContainer={showSearchContainer}
                    showingSearch={isSearch}
                    onSearch={(text) => {
                        setSearch(text);
                        
                    }} />
                    <AppRoutes />
                </MainContext.Provider>
            </div>
        </Provider>
    )

}


// /**
//  * We can start our initial App here in the main.js file
//  */
// class App extends React.Component {

//     /**
//      * Renders the default app in the window, we have assigned this to an element called root.
//      * 
//      * @returns JSX
//      * @memberof App
//     */
//     render() {
//         return (
            
//         );
//     }

// }

// Render this out
ReactDOM.render(<App />, document.getElementById('root'));
