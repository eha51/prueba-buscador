const stringSimilarity = require("string-similarity");
const data = require('./data');

module.exports = {
  findSearch:  (search) => {

    let results = [];
    let names = [];
  
    data.forEach((item) => {
      const umbral = stringSimilarity.compareTwoStrings(item.name,search);
      
      if(umbral >= 0.20) {
        results.push(item);
      }
  
      if(umbral > 0.10) {
        names.push(item);
      }
    })
  
    return {
      results,
      names
    };
  }
}